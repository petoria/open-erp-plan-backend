const {connect, get} = require("sequelize-dao");
const config = require('../config');
const path = require('path');
const fastify = require('fastify')({
    logger: true
});
fastify.use(require('cors')());

config.database.order = config.database.order.map(e => path.resolve(__dirname, e));
config.port = process.env.PORT || config.port;
config.database = process.env.DATABASE_URL || config.database;

const login_cb = require('./callbacks/login');
const users_create_cb = require('./callbacks/Users/create');
const users_delete_cb = require('./callbacks/Users/delete');
const users_get_cb = require('./callbacks/Users/get');

const deps_delete_cb = require('./callbacks/Departments/delete');
const deps_create_cb = require('./callbacks/Departments/create');
const deps_add_product_cb = require('./callbacks/Departments/add_product');
const deps_get_cb = require('./callbacks/Departments/get');

const prods_create_cb = require('./callbacks/Products/create');
const prods_delete_cb = require('./callbacks/Products/delete');
const prods_update_cb = require('./callbacks/Products/update');
const prods_get_cb = require('./callbacks/Products/get');

const clients_create_cb = require('./callbacks/Clients/create');
const clients_delete_cb = require('./callbacks/Clients/delete');
const clients_update_cb = require('./callbacks/Clients/update');
const clients_get_cb = require('./callbacks/Clients/get');


connect(config.database).then((connection) => {

    fastify.get("/hello", (req, res) => {
        res.send({msg: "PETORIA"})
    });

    fastify.post("/getAuth", login_cb.cb);

    fastify.post("/users/:username", users_create_cb.schema, users_create_cb.cb);
    fastify.delete("/users/:username", users_delete_cb.schema, users_delete_cb.cb);
    fastify.get("/users", users_get_cb.cb);

    fastify.post("/deps", deps_create_cb.schema, deps_create_cb.cb);
    fastify.delete("/deps", deps_delete_cb.schema, deps_delete_cb.cb);
    fastify.post("/deps/prods/:department", deps_add_product_cb.schema, deps_add_product_cb.cb);
    fastify.get("/deps", deps_get_cb.cb);


    fastify.post("/prods", prods_create_cb.schema, prods_create_cb.cb);
    fastify.delete("/prods/:code", prods_delete_cb.schema, prods_delete_cb.cb);
    fastify.put("/prods/:code", prods_update_cb.schema, prods_update_cb.cb);
    fastify.get("/prods", prods_get_cb.cb);

    fastify.post("/clients", clients_create_cb.schema, clients_create_cb.cb);
    fastify.delete("/clients/:code", clients_delete_cb.schema, clients_delete_cb.cb);
    fastify.put("/clients/:code", clients_update_cb.schema, clients_update_cb.cb);
    fastify.get("/clients", clients_get_cb.cb);


    fastify.listen(config.port, '0.0.0.0', function (err, address) {
        if (err) {
            fastify.log.error(err);
            process.exit(1)
        }
        fastify.log.info(`server listening on ${address}`)
    });
});