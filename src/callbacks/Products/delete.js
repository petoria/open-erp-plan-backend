const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")(async function (req, res) {
    get("Departments")
        .destroy({where: {code: req.params.code}})
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: [],
            properties: {}
        }
    }
};
