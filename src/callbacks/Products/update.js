const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")( async function (req, res) {
    let pr = await get("Departments").findAll({where: {code: req.params.code}});
    await Promise.all(pr.map(e => e.update(req.body)));
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: [],
            properties: {
                desc: {type: "string"},
                price:{type: "number"},
                production: {type: "boolean"},
                stored: {type: "number"},
                expose:{type: "boolean"}
            }
        }
    }
};
