const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")(async function (req, res) {
    get("Products")
        .build(req.body)
        .save()
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['code', 'desc', 'price', 'production', 'stored', 'expose'],
            properties: {
                code: {type: "string"},
                desc: {type: "string"},
                price: {type: "number"},
                production: {type: "boolean"},
                stored: {type: "number"},
                expose: {type: "boolean"}
            }
        }
    }
};
