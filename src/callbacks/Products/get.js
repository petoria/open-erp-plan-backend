const {withAuthAccess} = require("../../auth");
const {get} = require("sequelize-dao")

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")(async function (req, res) {
    return {list: await get("Products").findAll()}
});