const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")( async function (req, res) {
    req.body.privs.forEach(e => {
        get("UserAccesses")
            .build({username: req.params.username, level: e})
            .save()
    });
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['privs'],
            properties: {
                privs: {type: 'array'}
            }
        }
    }
};
