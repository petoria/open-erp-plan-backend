const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")(async function (req, res) {
    get("Departments")
        .build({dep_code: req.body.code, desc: req.body.desc})
        .save()
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['code', 'desc'],
            properties: {
                code: {type: 'string'},
                desc: {type: 'string'}
            }
        }
    }
};
