const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")(async function (req, res) {
    get("Productions")
        .build({dep_code: req.params.department, code: req.body.product_id, load: req.body.load})
        .save()
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['product_id'],
            properties: {
                product_id: {type: 'string'},
                load: {type: 'number'}
            }
        }
    }
};
