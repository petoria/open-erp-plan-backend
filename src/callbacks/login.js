const jwt = require("jsonwebtoken");
const fs = require("fs");
const config = require("../../config");
const crypto = require("crypto");

const privateKey = fs.readFileSync('./fingerprint/rsa-private.key');

module.exports.cb = async function (req, res) {
    let ip = (req.headers['x-forwarded-for'] || req.raw.connection.remoteAddress).split(".").map(e => parseInt(e));
    console.log(ip);
    let allowed = ip.map((e, i) => e & config.allowedPlanBackendNetworkMask[i]).reduce((a, b, i) => {
        return a && (b === (config.allowedPlanBackendNetwork[i] & config.allowedPlanBackendNetworkMask[i]))
    }, true);
    if (!allowed) {
        res.code(403);
        return {status: "forbidden"};
    }

    let data = req.body;
    let buffer = Buffer.from(data, "base64");
    let decrypted = crypto.privateDecrypt(privateKey, buffer).toString();
    let token = jwt.sign(
        JSON.parse(decrypted.toString()),
        privateKey,
        {algorithm: 'RS256', expiresIn: "8h"}
    );
    return {token: token};
};
