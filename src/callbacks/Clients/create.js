const {connect, get} = require("sequelize-dao");
const {withAuthAccess} = require("../../auth");

module.exports.cb = withAuthAccess("AMMINISTRAZIONE")(async function (req, res) {
    get("Products")
        .build(req.body)
        .save();
    return {status: "ok"}
});

module.exports.schema = {
    schema: {
        body: {
            type: 'object',
            required: ['name', 'cf', 'address', 'notes', 'trust'],
            properties: {
                name: {type: "string"},
                cf: {type: "string"},
                address: {type: "string"},
                notes: {type: "string"},
                trust: {type: "number"}
            }
        }
    }
};
