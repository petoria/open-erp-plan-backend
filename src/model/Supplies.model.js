const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Products",
                key: "code"
            }
        },
        piva: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Suppliers",
                key: "piva"
            }
        },
        foreginCode: {
            type: Sequelize.STRING,
            allowNull: false
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            piva: "06645501085",
            code: "RG",
            foreginCode: "01448216"
        },
        {
            piva: "59654852358",
            code: "RG",
            foreginCode: "01448216"
        },
        {
            piva: "59654852358",
            code: "MN-01",
            foreginCode: "01448216"
        },
        {
            piva: "59654852358",
            code: "MN-02",
            foreginCode: "01448216"
        },
        {
            piva: "06645501085",
            code: "GR-11",
            foreginCode: "01448216"
        },
        {
            piva: "06645501085",
            code: "GR-18",
            foreginCode: "01448216"
        },
        {
            piva: "06645501085",
            code: "GR-23",
            foreginCode: "01448216"
        },
        {
            piva: "06645501085",
            code: "GR-32",
            foreginCode: "01448216"
        },
        {
            piva: "94896582655",
            code: "V-01",
            foreginCode: "01448216"
        },
        {
            piva: "94896582655",
            code: "V-02",
            foreginCode: "01448216"
        },
    ]
};