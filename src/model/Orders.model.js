const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        cid: {
            type: Sequelize.STRING,
            primaryKey: true,
            unique: "cid_u"
        },
        client: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Clients",
                key: "cf"
            }
        },
        price: {
            type: Sequelize.BIGINT(11)
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [

    ]
};

//         {
//             cid: "aa93e8",
//             code: "PTTENR56M28F856E",
//             price: "2540"
//         },
//         {
//             cid: "df85e9",
//             code: "BNFTTV67J04G986E",
//             price: "6958"
//         },
//         {
//             cid: "ds69e8",
//             code: "PRCPSQ89J11G854D",
//             price: "3254"
//         },