const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        code: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            unique: "dep_u"
        },
        desc: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        price:{
            type: Sequelize.BIGINT(11),
            allowNull: false
        },
        production: {
            type: Sequelize.BOOLEAN, //1 prod esterna, 0 interna
            allowNull: false
        },
        stored: {
            type: Sequelize.BIGINT(11),
            allowNull: false,
            default: 0
        },
        expose:{
            type: Sequelize.BOOLEAN,
            allowNull: false
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            code: "A1-06001",
            desc: "Bicicletta donna, 6 rapporti - BIANCO",
            price: 0,
            production: 0,
            stored: 1,
            expose: 0
        },
        {
            code: "A1-01002",
            desc: "Bicicletta donna, no cambio - ROSSO",
            price: 0,
            production: 0,
            stored: 0,
            expose: 0
        },
        {
            code: "A2-09001",
            desc: "Bicicletta uomo, 9 rapporti - BIANCO",
            price: 0,
            production: 0,
            stored: 0,
            expose: 0
        },
        {
            code: "A2-01002",
            desc: "Bicicletta uomo, no cambio - ROSSO",
            price: 0,
            production: 0,
            stored: 0,
            expose: 0
        },

        {
            code: "A1-06001-C",
            desc: "Bicicletta donna, 6 rapporti - BIANCO - collaudato",
            price: 25000,
            production: 0,
            stored: 0,
            expose: 1
        },
        {
            code: "A1-01002-C",
            desc: "Bicicletta donna, no cambio - ROSSO - collaudato",
            price: 20000,
            production: 0,
            stored: 2,
            expose: 1
        },
        {
            code: "A2-09001-C",
            desc: "Bicicletta uomo, 9 rapporti - BIANCO - collaudato",
            price: 30000,
            production: 0,
            stored: 0,
            expose: 1
        },
        {
            code: "A2-01002-C",
            desc: "Bicicletta uomo, no cambio - ROSSO - collaudato",
            price: 29000,
            production: 0,
            stored: 0,
            expose: 1
        },

        {
            code: "TL-01",
            desc: "Telaio donna",
            price: 0,
            production: 0,
            stored: 0,
            expose: 0
        },
        {
            code: "TL-02",
            desc: "Telaio uomo",
            price: 0,
            production: 0,
            stored: 0,
            expose: 0
        },
        {
            code: "TL-01001",
            desc: "Telaio donna BIANCO",
            price: 26000,
            production: 0,
            stored: 0,
            expose: 1
        },
        {
            code: "TL-01002",
            desc: "Telaio uomo ROSSO",
            price: 26000,
            production: 0,
            stored: 0,
            expose: 1
        },
        {
            code: "TL-02001",
            desc: "Telaio donna BIANCO",
            price: 26000,
            production: 0,
            stored: 0,
            expose: 1
        },
        {
            code: "TL-02002",
            desc: "Telaio uomo ROSSO",
            price: 26000,
            production: 0,
            stored: 0,
            expose: 1
        },

        {
            code: "MN-01",
            desc: "Manubrio typ.1",
            price: 3560,
            production: 1,
            stored: 0,
            expose: 1
        },
        {
            code: "MN-02",
            desc: "Manubrio typ.2",
            price: 3560,
            production: 1,
            stored: 0,
            expose: 1
        },

        {
            code: "V-01",
            desc: "Vernice bianca",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        },
        {
            code: "V-02",
            desc: "Vernice rossa",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        },

        {
            code: "CMB-06",
            desc: "Cambio 6 rapporti",
            price: 5000,
            production: 0,
            stored: 0,
            expose: 1
        },
        {
            code: "CMB-09",
            desc: "Cambio 9 rapporti",
            price: 7050,
            production: 0,
            stored: 0,
            expose: 1
        },

        {
            code: "GR-11",
            desc: "Condotta 11 denti",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        },
        {
            code: "GR-18",
            desc: "Condotta 18 denti",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        },
        {
            code: "GR-23",
            desc: "Condotta 23 denti",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        },
        {
            code: "GR-32",
            desc: "Condotta 32 denti",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        },

        {
            code: "RT",
            desc: "Ruota",
            price: 1500,
            production: 0,
            stored: 0,
            expose: 1
        },

        {
            code: "RG",
            desc: "Raggio",
            price: 0,
            production: 1,
            stored: 0,
            expose: 0
        }
    ]
};