const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        dep_code: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
            unique: "dep_u"
        },
        desc: {
            type: Sequelize.STRING,
            allowNull: false,
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            dep_code: "CF",
            desc: "Collaudo"
        },
        {
            dep_code: "IF",
            desc: "Imballaggio"
        },
        {
            dep_code: "AF1",
            desc: "Assemblaggio1"
        },
        {
            dep_code: "SLDMNT1",
            desc: "Saldatura1"
        },
        {
            dep_code: "AF2",
            desc: "Assemblaggio2"
        },
        {
            dep_code: "SLDMNT2",
            desc: "Saldatura2"
        },
        {
            dep_code: "MC",
            desc: "MontaggioCambi"
        },
        {
            dep_code: "V",
            desc: "Verniciatura"
        }
    ]
};