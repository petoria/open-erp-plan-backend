const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Products",
                key: "code"
            }
        },
        dep_code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Departments",
                key: "dep_code"
            }
        },
        load: {
            type: Sequelize.BIGINT(11)//1/10000
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            code: "A1-06001",
            dep_code: "AF1",
            load: 100
        },
        {
            code: "A1-06001",
            dep_code: "AF2",
            load: 50
        },
        {
            code: "A1-01002",
            dep_code: "AF1",
            load: 100
        },
        {
            code: "A2-09001",
            dep_code: "AF1",
            load: 100
        },
        {
            code: "A2-01002",
            dep_code: "AF1",
            load: 100
        },

        {
            code: "A1-06001-C",
            dep_code: "CF",
            load: 80
        },
        {
            code: "A1-01002-C",
            dep_code: "CF",
            load: 80
        },
        {
            code: "A2-09001-C",
            dep_code: "CF",
            load: 80
        },
        {
            code: "A2-01002-C",
            dep_code: "CF",
            load: 80
        },

        {
            code: "TL-01",
            dep_code: "SLDMNT1",
            load: 50
        },
        {
            code: "TL-01",
            dep_code: "SLDMNT2",
            load: 50
        },
        {
            code: "TL-02",
            dep_code: "SLDMNT1",
            load: 50
        },
        {
            code: "TL-02",
            dep_code: "SLDMNT2",
            load: 50
        },

        {
            code: "TL-01001",
            dep_code: "V",
            load: 60
        },
        {
            code: "TL-01002",
            dep_code: "V",
            load: 60
        },
        {
            code: "TL-02001",
            dep_code: "V",
            load: 65
        },
        {
            code: "TL-02002",
            dep_code: "V",
            load: 65
        },

        {
            code: "CMB-06",
            dep_code: "MC",
            load: 1000
        },
        {
            code: "CMB-09",
            dep_code: "MC",
            load: 1200
        },

        {
            code: "RT",
            dep_code: "AF2",
            load: 1000
        },
    ]
};