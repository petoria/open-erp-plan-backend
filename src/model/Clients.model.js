const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        cf: {
            type: Sequelize.STRING,
            primaryKey: true,
            unique: "piva_u"
        },
        address: {
            type: Sequelize.STRING
        },
        notes: {
            type: Sequelize.STRING
        },
        trust: {
            type: Sequelize.BIGINT(11)
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            name: "Pasquale Porceddu",
            piva: "PRCPSQ89J11G854D",
            address: "Via Fratelli Bonaldi, 3, Cagliari",
            notes: "Terrone",
            trust: 4
        },
        {
            name: "Enrico Patata",
            piva: "PTTENR56M28F856E",
            address: "Via Matriosca, 11, Mosca",
            notes: "Comunista",
            trust: 8
        },
        {
            name: "Bonifacio VIII",
            piva: "BNFTTV67J04G986E",
            address: "Via Paolo VI, 3, Roma",
            notes: "Papa",
            trust: 6
        },
    ]
};