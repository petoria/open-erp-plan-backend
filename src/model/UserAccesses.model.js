const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        username: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        level: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            references: {
                model: 'Accesses',
                key: "level"
            }
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            username: "tetofonta",
            level: 0
        },
        {
            username: "tetofonta",
            level: 1
        },
        {
            username: "vezzoo",
            level: 1
        }
    ]
};