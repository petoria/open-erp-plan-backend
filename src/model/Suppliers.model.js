const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        piva: {
            type: Sequelize.STRING,
            primaryKey: true,
            unique: "piva_u"
        },
        address: {
            type: Sequelize.STRING
        }
    };
};

module.exports.registered = function (table) {
    //check piva
};

module.exports.default_data = function () {
    return [
        {
            name: "Wheel S.R.L.",
            piva: "06645501085",
            address: "Via Fratelli Bonaldi, 3, Brescia"
        },
        {
            name: "OML S.R.L.",
            piva: "59654852358",
            address: "Via Cesare Battisti, 14, Lumezzane"
        },
        {
            name: "Vernici S.P.A.",
            piva: "94896582655",
            address: "Via Milano, 92, Palazzolo sull'Oglio"
        }
    ]
};