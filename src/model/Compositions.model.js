const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Products",
                key: "code"
            }
        },
        dep: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Products",
                key: "code"
            }
        },
        qta: {
            type: Sequelize.BIGINT(11)
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            code: "A1-06001-C",
            dep: "A1-06001",
            qta: 1
        },
        {
            code: "A1-01002-C",
            dep: "A1-01002",
            qta: 1
        },
        {
            code: "A2-09001-C",
            dep: "A2-09001",
            qta: 1
        },
        {
            code: "A2-01002-C",
            dep: "A2-01002",
            qta: 1
        },

        {
            code: "A1-06001",
            dep: "TL-01001",
            qta: 1
        },
        {
            code: "A1-06001",
            dep: "CMB-06",
            qta: 1
        },
        {
            code: "A1-06001",
            dep: "MN-01",
            qta: 1
        },
        {
            code: "A1-06001",
            dep: "RT",
            qta: 2
        },

        {
            code: "A1-01002",
            dep: "TL-01002",
            qta: 1
        },
        {
            code: "A1-01002",
            dep: "GR-11",
            qta: 1
        },
        {
            code: "A1-01002",
            dep: "MN-01",
            qta: 1
        },
        {
            code: "A1-01002",
            dep: "RT",
            qta: 2
        },

        {
            code: "A2-09001",
            dep: "TL-02001",
            qta: 1
        },
        {
            code: "A2-09001",
            dep: "CMB-09",
            qta: 1
        },
        {
            code: "A2-09001",
            dep: "MN-02",
            qta: 1
        },
        {
            code: "A2-09001",
            dep: "RT",
            qta: 2
        },

        {
            code: "A2-01002",
            dep: "TL-02002",
            qta: 1
        },
        {
            code: "A2-01002",
            dep: "GR-23",
            qta: 1
        },
        {
            code: "A2-01002",
            dep: "MN-02",
            qta: 1
        },
        {
            code: "A2-01002",
            dep: "RT",
            qta: 2
        },

        {
            code: "TL-01001",
            dep: "TL-01",
            qta: 1
        },
        {
            code: "TL-01001",
            dep: "V-01",
            qta: 15
        },
        {
            code: "TL-01002",
            dep: "TL-02",
            qta: 1
        },
        {
            code: "TL-01002",
            dep: "V-02",
            qta: 15
        },
        {
            code: "TL-02001",
            dep: "TL-02",
            qta: 1
        },
        {
            code: "TL-02001",
            dep: "V-01",
            qta: 15
        },
        {
            code: "TL-02002",
            dep: "TL-02",
            qta: 1
        },
        {
            code: "TL-02002",
            dep: "V-02",
            qta: 15
        },

        {
            code: "CMB-06",
            dep: "GR-11",
            qta: 2
        },
        {
            code: "CMB-09",
            dep: "GR-11",
            qta: 2
        },
        {
            code: "RT",
            dep: "RG",
            qta: 36
        }

    ]
};