const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        dep_code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Departments",
                key: "dep_code"
            }
        },
        code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Products",
                key: "code"
            }
        },
        qta: {
            type: Sequelize.BIGINT(11)
        },
        productiondate: {
            type: Sequelize.BIGINT(11)
        },
        cid: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Orders",
                key: "cid"
            }
        },
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [

    ]
};