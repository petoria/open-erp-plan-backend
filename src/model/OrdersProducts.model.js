const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        cpid: {
            type: Sequelize.STRING,
            primaryKey: true,
            unique: "cpid_u"
        },
        cid: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Orders",
                key: "cid"
            }
        },
        code: {
            type: Sequelize.STRING,
            allowNull: false,
            references: {
                model: "Products",
                key: "code"
            }
        },
        qta: {
            type: Sequelize.BIGINT(11)
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [

    ]
};


//         {
//             cpid: "ni08j8",
//             code: "A2-09001-C",
//             cid: "aa93e8",
//             qta: 2
//         },
//         {
//             cpid: "so39o0",
//             code: "MN-01",
//             cid: "aa93e8",
//             qta: 1
//         },
//         {
//             cpid: "ws89r5",
//             code: "A1-01002-C",
//             cid: "df85e9",
//             qta: 5
//         },
//         {
//             cpid: "gc59w6",
//             code: "MN-02",
//             cid: "ds69e8",
//             qta: 4
//         },
//         {
//             cpid: "qb91e6",
//             code: "CMB-09",
//             cid: "ds69e8",
//             qta: 3
//         },