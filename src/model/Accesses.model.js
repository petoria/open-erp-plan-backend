const Sequelize = require("sequelize");

module.exports.register = function () {
    return {
        level: {
            type: Sequelize.BIGINT(11),
            primaryKey: true,
        },
        desc: {
            type: Sequelize.STRING
        }
    };
};

module.exports.registered = function (table) {

};

module.exports.default_data = function () {
    return [
        {
            level: 0,
            desc: "AMMINISTRAZIONE"
        },
        {
            level: 1,
            desc: "MAGAZZINO"
        }
    ]
};