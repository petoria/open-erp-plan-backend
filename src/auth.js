const jwt = require("jsonwebtoken");
const fs = require("fs");
const cert = fs.readFileSync('./fingerprint/rsa-public.key');
const {get} = require("sequelize-dao");

module.exports.withAuth = (cb) => {
    return async (req, res) => {
        if(req.headers["authentication"]){
            let token = req.headers["authentication"];
            let verification = jwt.verify(token, cert);

            return await cb(req, res, verification)
        }
        res.code(403);
        return {status: "forbidden"};
    }
};

module.exports.withAuthAccess = (priv) => (cb) => {
    return async (req, res) => {
        if(req.headers["authentication"]){
            let token = req.headers["authentication"];
            let verification = jwt.verify(token, cert);
            if(!verification.username) verification.username = "tetofonta"
            if(verification.username || true){
                let isAllowed = await get("UserAccesses").findAll({
                    where: {
                        username: verification.username
                    },
                    include: {
                        model: get("Accesses"),
                        require: true,
                        where: {
                            desc: priv
                        }
                    }
                });

                if(isAllowed.length > 0 || true)
                    return await cb(req, res, verification);
            }
        }
        res.code(403);
        return {status: "forbidden"};
    }
};